package com.tapsell.mediator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.tapsell.adsdk.mediator.Mediator
import com.tapsell.adsdk.mediator.RequestAdListener
import com.tapsell.adsdk.mediator.ShowAdListener
import com.tapsell.adsdk.utils.Utils

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.ad).setOnClickListener {
            Mediator.requestAd("", object : RequestAdListener {
                override fun onRequestAdError(message: String) {
                    Utils.toast("error : $message",baseContext)
                }

                override fun onRequestAdComplete() {
                    Utils.toast("REQUEST DONE !",baseContext)

                    Utils.debug("REQUEST DONE !")
                }

            })
        }

        findViewById<Button>(R.id.show_ad).setOnClickListener {
            Mediator.showAd(
                activity = this,
                showAdListener = object : ShowAdListener {
                    override fun onError(message: String) {
                        Log.d("MAIN_ACTIVITY", message)
                        Utils.toast(message,baseContext)

                    }

                    override fun onOpened() {
                        Log.d("MAIN_ACTIVITY", "OPEN")
                        Utils.toast("OPEN",baseContext)

                    }

                    override fun onClosed() {
                        Log.d("MAIN_ACTIVITY", "CLOSE")
                        Utils.toast("CLOSE",baseContext)

                    }

                    override fun onRewarded(reward: Boolean) {

                        Log.d("MAIN_ACTIVITY", "REWARD = $reward+")
                        Utils.toast("REWARD = $reward+",baseContext)

                    }


                })

        }

        findViewById<Button>(R.id.init_ad).setOnClickListener {
            Mediator.initialize(
                "a",
                context = applicationContext,
                "dbb5513f0725ff44885ec2b4aab6d3c008866c38"
            )

        }


    }
}