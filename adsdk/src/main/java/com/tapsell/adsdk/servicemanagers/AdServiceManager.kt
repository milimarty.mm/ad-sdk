package com.tapsell.adsdk.servicemanagers

import android.app.Activity
import android.content.Context
import com.tapsell.adsdk.mediator.InitializerResponse
import com.tapsell.adsdk.mediator.RequestAdListener
import com.tapsell.adsdk.mediator.ShowAdListener
import com.tapsell.adsdk.utils.AppConfig
import com.tapsell.adsdk.utils.Utils


class AdServiceManager {

    fun initialize(
        appId: String,
        context: Context,
        chartboostAppSignature: String,
        listener: InitializerResponse? = null
    ) {
        /**
         * init application context
         */
        AppConfig.gContext = context

        if (!Utils.haveNetworkConnection()) {
            listener?.onError("NO Internet connection")
            return
        }
        AdInitializerManager.initialize(appId, context, chartboostAppSignature, listener)
    }

    fun requestAd(zoneId: String, requestAdListener: RequestAdListener) {
        if (!AppConfig.isSdkServiceInitialized) {
            requestAdListener.onRequestAdError("Please call initialize method first")
            return
        }
        if (!Utils.haveNetworkConnection()) {
            requestAdListener.onRequestAdError("NO Internet connection")
            return
        }
        AdRequestManager.requestAd(zoneId, requestAdListener)
    }

    fun showAd(zoneId: String? = null, activity: Activity, showAdListener: ShowAdListener) {
        if (!AppConfig.isSdkServiceInitialized) {
            showAdListener.onError("Please call initialize method first")
            return
        }
        if (!Utils.haveNetworkConnection()) {
            showAdListener.onError("NO Internet connection")
            return
        }
        ShowAdManager.showAd(zoneId, activity, showAdListener)
    }

}