package com.tapsell.adsdk.servicemanagers

import android.app.Activity
import com.tapsell.adsdk.adnetworks.AdServiceGen
import com.tapsell.adsdk.api.repository.ServicesRepository
import com.tapsell.adsdk.mediator.ShowAdListener
import com.tapsell.adsdk.utils.AppConfig

internal object ShowAdManager {

    /**
     *
     * STEEP 3 SHOW AD
     */

    fun showAd(zoneId: String? = null, activity: Activity, showAdListener: ShowAdListener) {

        ServicesRepository.getAllByIsAdReady {
            it?.let { waterfall ->
                zoneId?.let {
                    waterfall.zoneId = zoneId
                }
                if (waterfall.isAdReady) {
                    AdServiceGen.getAdService(waterfall.adNetwork).showAd(
                        showAdListener,
                        waterfall,
                        activity
                    )
                }
            } ?: kotlin.run {
                showAdListener.onError("there is no ready AD")
            }
        }

    }

}