package com.tapsell.adsdk.servicemanagers

import android.app.Application
import android.content.Context
import com.tapsell.adsdk.adnetworks.AdServiceGen
import com.tapsell.adsdk.api.model.AdNetworksModel
import com.tapsell.adsdk.api.repository.ResponseListener
import com.tapsell.adsdk.api.repository.ServicesRepository
import com.tapsell.adsdk.mediator.InitializerResponse
import com.tapsell.adsdk.utils.AppConfig
import com.tapsell.adsdk.utils.Utils

internal object AdInitializerManager {

    private var initializerListener: InitializerResponse =
        object : InitializerResponse {
            override fun onComplete() {
            }

            override fun onError(message: String) {
            }

        }


    /**
     * STEEP 1 INITIALIZER
     */

    /**
     * this method will init sdk
     */

    @JvmOverloads
    fun initialize(
        appId: String,
        context: Context,
        chartboostAppSignature: String,
        listener: InitializerResponse? = null
    ) {
        listener?.let {
            initializerListener = it
        }

        if (context !is Application) {
            initializerListener.onError("Please Send Application Context")
            Utils.error("Please Send Application Context")
            Utils.toast("Please Send Application Context", context)

            return
        }


        ServicesRepository.getInitializeConfig(
            appId,
            chartboostAppSignature,
            object : ResponseListener<AdNetworksModel> {
                override fun onSuccess(data: AdNetworksModel) {
                    startInitSdks(data.adAdNetworksAppIdModel,chartboostAppSignature)
                    AppConfig.isSdkServiceInitialized = true
                    Utils.debug("Init done !!")
                    Utils.toast("Init done !!")

                }

                override fun onError(message: String) {
                    Utils.error(message)
                    Utils.toast("error : $message")
                    initializerListener.onError(message)
                }
            })


    }

    private fun startInitSdks(
        adNetworksModel: AdNetworksModel.AdNetworksAppIdModel,
        chartboostAppSignature: String
    ) {
        val initializerResponse = object : InitializerResponse {
            override fun onComplete() {

            }

            override fun onError(message: String) {
                initializerListener.onError(message)
            }
        }

        adNetworksModel.serviceApIdList.forEach { pair ->
            pair.second?.let {
                AdServiceGen.getAdService(pair.first.name).apply {
                    appSignature = chartboostAppSignature
                    initialize(it, initializerResponse)
                }

            }
        }


    }


}