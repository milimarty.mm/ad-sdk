package com.tapsell.adsdk.servicemanagers

import com.tapsell.adsdk.adnetworks.AdServiceGen
import com.tapsell.adsdk.api.model.RequestAdModel
import com.tapsell.adsdk.api.repository.ResponseListener
import com.tapsell.adsdk.api.repository.ServicesRepository
import com.tapsell.adsdk.mediator.RequestAdListener
import com.tapsell.adsdk.utils.AppConfig
import com.tapsell.adsdk.utils.Utils
import com.unity3d.services.core.misc.Utilities
import kotlinx.coroutines.*

internal object AdRequestManager {
    /**
     * RequestAd job
     */
    private lateinit var requestAdJob: CompletableJob
    private var isRequestAdJobActive = false
    private lateinit var requestAdListener: RequestAdListener


    private fun adRequestJobComplete(adId: String, adNetwork: String) {
        requestAdJob.cancelChildren()
        requestAdJob.complete()
        ServicesRepository.updateWaterfallByAdNetworkId(adId, adNetwork)

    }

    fun requestAd(zoneId: String, requestAdListener: RequestAdListener) {
        AdRequestManager.requestAdListener = requestAdListener


        ServicesRepository.getAdRequestConfig(
            zoneId,
            result = object : ResponseListener<RequestAdModel> {
                override fun onSuccess(data: RequestAdModel) {
                    startAdRequest(data.zoneType, data.waterfallModel)
                    Utils.debug(data)
                }

                override fun onError(message: String) {
                    Utils.error(message)
                    AppConfig.gContext
                    Utilities.runOnUiThread {
                        requestAdListener.onRequestAdError(message)
                    }

                }
            })

    }


    private fun startAdRequest(
        zoneType: String,
        waterfallModelRequest: List<RequestAdModel.WaterfallModel>
    ) {
        if (!isRequestAdJobActive) {
            requestAdJob = Job()
            CoroutineScope(Dispatchers.Default + requestAdJob).launch {
                isRequestAdJobActive = true
                waterfallModelRequest.forEach {
                    withTimeout(it.timeout.toLong()) {
                        async {
                            AdServiceGen.getAdService(it.adNetwork).apply {
                                if (isServiceActive)
                                    requestAd(
                                        zoneType,
                                        it,
                                        ::adRequestJobComplete, requestAdJob
                                    )
                            }
                        }
                    }.await()
                    delay(it.timeout.toLong())
                }
                requestAdJob.completeExceptionally(Exception("No AdNetworks is Available"))
            }
        }

        requestAdJob.invokeOnCompletion { exception ->
            if (isRequestAdJobActive) {
                isRequestAdJobActive = false
                exception?.let {
                    Utils.error("JOBS DONE !!")
                    Utilities.runOnUiThread {
                        requestAdListener.onRequestAdError(it.message.toString())
                    }
                    Utils.error(it.message.toString())
                    return@invokeOnCompletion
                }
                Utilities.runOnUiThread {
                    requestAdListener.onRequestAdComplete()
                }
                Utils.debug("JOBS DONE")
                if (requestAdJob.isActive) {
                    requestAdJob.cancel()
                }
            }

        }
    }


}