package com.tapsell.adsdk.adnetworks.chartboostsdk

import android.app.Activity
import com.chartboost.sdk.CBLocation
import com.chartboost.sdk.Chartboost
import com.chartboost.sdk.ChartboostDelegate
import com.chartboost.sdk.Model.CBError
import com.tapsell.adsdk.mediator.ShowAdListener
import com.tapsell.adsdk.adnetworks.AdService
import com.tapsell.adsdk.adnetworks.AdServiceEnum
import com.tapsell.adsdk.api.model.RequestAdModel
import com.tapsell.adsdk.mediator.InitializerResponse
import com.tapsell.adsdk.utils.Utils
import kotlinx.coroutines.CompletableJob

internal class ChartboostService() :
    AdService(AdServiceEnum.Chartboost) {

    override fun initialize(appId: String, initializerResponse: InitializerResponse) {
        Chartboost.startWithAppId(context, appId, appSignature)
        Chartboost.setDelegate(object : ChartboostDelegate() {
            override fun didInitialize() {
                isServiceActive = true
                initializerResponse.onComplete()
            }

        })

    }

    override fun requestAd(
        zoneType: String,
        waterfallModelRequest: RequestAdModel.WaterfallModel,
        onJobComplete: (String, String) -> Unit,
        job: CompletableJob
    ) {
        if (Chartboost.isSdkStarted()) {
            init(zoneType = zoneType)
            if (isRewarded)
                Chartboost.cacheRewardedVideo(CBLocation.LOCATION_MAIN_MENU)
            else
                Chartboost.cacheInterstitial(CBLocation.LOCATION_MAIN_MENU)
            if (job.isActive)
                onJobComplete(currentService, currentService)

        }


        Chartboost.setDelegate(object : ChartboostDelegate() {
            override fun didCacheInterstitial(location: String?) {


            }

            override fun didCacheRewardedVideo(location: String?) {

            }
        })
    }

    override fun showAd(
        showAdListener: ShowAdListener,
        waterfallModel: RequestAdModel.WaterfallModel,
        activity: Activity
    ) {
        Chartboost.setDelegate(object : ChartboostDelegate() {
            override fun didFailToLoadInterstitial(
                location: String?,
                error: CBError.CBImpressionError?
            ) {
                Utils.error(error?.name ?: "Unknown Error in Chartboost")
                showAdListener.onError(error?.name ?: "Unknown Error in Chartboost")
            }

            override fun willDisplayVideo(location: String?) {
                showAdListener.onOpened()
                adShown(waterfallModel)
            }

            override fun willDisplayInterstitial(location: String?) {
                showAdListener.onOpened()
                adShown(waterfallModel)

            }

            override fun didCloseRewardedVideo(location: String?) {
                showAdListener.onError("Cannot load Chartboost RewardedVideo")

            }

            override fun didCloseInterstitial(location: String?) {
                showAdListener.onError("Cannot load Chartboost Interstitial")
            }
        })
        if (isRewarded) {
            if (Chartboost.hasRewardedVideo(CBLocation.LOCATION_MAIN_MENU)) {
                Chartboost.showRewardedVideo(CBLocation.LOCATION_MAIN_MENU)
            } else {
                Chartboost.cacheRewardedVideo(CBLocation.LOCATION_MAIN_MENU)
                Chartboost.showRewardedVideo(CBLocation.LOCATION_MAIN_MENU)

            }
        } else
            if (Chartboost.hasInterstitial(CBLocation.LOCATION_MAIN_MENU)) {
                Chartboost.showInterstitial(CBLocation.LOCATION_MAIN_MENU)
            } else {
                Chartboost.cacheInterstitial(CBLocation.LOCATION_MAIN_MENU)
                Chartboost.showInterstitial(CBLocation.LOCATION_MAIN_MENU)
            }


    }

}