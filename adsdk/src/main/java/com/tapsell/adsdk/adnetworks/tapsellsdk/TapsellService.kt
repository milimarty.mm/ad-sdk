package com.tapsell.adsdk.adnetworks.tapsellsdk

import android.app.Activity
import com.tapsell.adsdk.mediator.ShowAdListener
import com.tapsell.adsdk.adnetworks.AdService
import com.tapsell.adsdk.adnetworks.AdServiceEnum
import com.tapsell.adsdk.api.model.RequestAdModel
import com.tapsell.adsdk.mediator.InitializerResponse
import com.tapsell.adsdk.utils.Utils
import ir.tapsell.sdk.*
import kotlinx.coroutines.CompletableJob

internal class TapsellService :
    AdService(AdServiceEnum.Tapsell) {

    override fun initialize(appId: String, initializerResponse: InitializerResponse) {
        Tapsell.initialize(context, appId)
        isServiceActive = true
        initializerResponse.onComplete()
    }

    override fun requestAd(
        zoneType: String,
        waterfallModelRequest: RequestAdModel.WaterfallModel,
        onJobComplete: (String, String) -> Unit, job: CompletableJob
    ) {
       val tapsellAdRequestOption = TapsellAdRequestOptions().apply {
           cacheType = TapsellAdRequestOptions.CACHE_TYPE_CACHED
       }

        Tapsell.requestAd(context,
            waterfallModelRequest.zoneId, tapsellAdRequestOption,
            object : TapsellAdRequestListener() {
                override fun onAdAvailable(adId: String?) {
                    if (job.isActive){
                        adId?.let {
                            Utils.debug(it)
                        }
                        onJobComplete(adId ?: currentService, currentService)
                    }
                }


                override fun onError(message: String?) {
                    Utils.error(message.toString())
                }


            }
        )
    }

    override fun showAd(
        showAdListener: ShowAdListener,
        waterfallModel: RequestAdModel.WaterfallModel,
        activity: Activity
    ) {
        Tapsell.showAd(context, waterfallModel.zoneId, waterfallModel.adId,
            TapsellShowOptions(),
            object : TapsellAdShowListener() {
                override fun onOpened() {
                    showAdListener.onOpened()
                    adShown(waterfallModel)

                }

                override fun onClosed() {
                    showAdListener.onClosed()

                }

                override fun onClosed(p0: TapsellAd?) {
                    showAdListener.onClosed()


                }

                override fun onError(p0: String?) {
                    showAdListener.onError(p0 ?: "Unknown Error")
                    adShown(waterfallModel)

                }

                override fun onRewarded(p0: Boolean) {
                    showAdListener.onRewarded(p0)

                }
            }
        )

    }

}