package com.tapsell.adsdk.adnetworks

import android.app.Activity
import android.app.Application
import com.tapsell.adsdk.api.model.RequestAdModel
import com.tapsell.adsdk.api.repository.ServicesRepository
import com.tapsell.adsdk.mediator.InitializerResponse
import com.tapsell.adsdk.mediator.ShowAdListener
import com.tapsell.adsdk.utils.AppConfig
import kotlinx.coroutines.CompletableJob

internal abstract class AdService(
    adServiceEnum: AdServiceEnum, var appSignature: String = ""
) {
    /**
     * service status
     */
    var isServiceActive: Boolean = false

    /**
     * appId or token for init sdk
     */
    var appId: String = ""

    var zoneType: String? = null

    /**
     * getContext
     */
    var context = AppConfig.gContext as Application

    /**
     * Current sdk name
     */
    var currentService: String = adServiceEnum.name

    /**
     * check zoneType is Rewarded or not
     */
    val isRewarded: Boolean
        get() = zoneType != null


    /**
     * for initialize sdk you must call initialize method first
     * use this method for init your sdk
     */
    abstract fun initialize(appId: String, initializerResponse: InitializerResponse)

    /**
     *  get request init by calling this method
     */

    abstract fun requestAd(
        zoneType: String,
        waterfallModelRequest: RequestAdModel.WaterfallModel,
        onJobComplete: (String, String) -> Unit = { _, _ -> },
        job: CompletableJob
    )

    abstract fun showAd(
        showAdListener: ShowAdListener,
        waterfallModel: RequestAdModel.WaterfallModel,
        activity: Activity
    )


    /**
     * call this method when add finished or when get error
     */
    fun adShown(waterfallMode: RequestAdModel.WaterfallModel) {
        waterfallMode.apply {
            adId = ""
            isAdReady = false
            ServicesRepository.updateWaterFall(this)

        }

    }

    fun init(appId: String? = null, zoneType: String? = null) {
        appId?.let {
            this.appId = appId
        }
        zoneType?.let {
            this.zoneType = it
        }
    }


}