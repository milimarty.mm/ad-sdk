package com.tapsell.adsdk.adnetworks

internal enum class AdServiceEnum(type:String) {
    Tapsell("Tapsell"), UnityAds("UnityAds"), Chartboost("Chartboost")
}