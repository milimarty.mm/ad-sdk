package com.tapsell.adsdk.adnetworks.unityadsdk

import android.app.Activity
import com.tapsell.adsdk.mediator.ShowAdListener
import com.tapsell.adsdk.adnetworks.AdService
import com.tapsell.adsdk.adnetworks.AdServiceEnum
import com.tapsell.adsdk.api.model.RequestAdModel
import com.tapsell.adsdk.mediator.InitializerResponse
import com.tapsell.adsdk.utils.Utils
import com.unity3d.ads.IUnityAdsInitializationListener
import com.unity3d.ads.IUnityAdsListener
import com.unity3d.ads.UnityAds
import kotlinx.coroutines.CompletableJob

internal class UnityAdService :
    AdService(AdServiceEnum.UnityAds) {

    override fun initialize(appId: String, initializerResponse: InitializerResponse) {
        init(appId = appId)
        UnityAds.initialize(context, appId, object : IUnityAdsInitializationListener {
            override fun onInitializationComplete() {
                isServiceActive = true
                initializerResponse.onComplete()
            }

            override fun onInitializationFailed(
                p0: UnityAds.UnityAdsInitializationError?,
                p1: String?
            ) {
                initializerResponse.onError(p0?.name ?: "Unknown Error in UnityAds Initialization")

            }
        })
    }

    override fun requestAd(
        zoneType: String,
        waterfallModelRequest: RequestAdModel.WaterfallModel,
        onJobComplete: (String, String) -> Unit,
        job: CompletableJob
    ) {
        init(zoneType = zoneType)

        if (UnityAds.isReady()&&job.isActive)
            onJobComplete(currentService, currentService)
        else
            Utils.error("UnityService is not ready")
    }

    override fun showAd(
        showAdListener: ShowAdListener,
        waterfallModel: RequestAdModel.WaterfallModel,
        activity: Activity
    ) {
        UnityAds.show(activity)
        UnityAds.addListener(object : IUnityAdsListener {
            override fun onUnityAdsReady(p0: String?) {

            }

            override fun onUnityAdsStart(p0: String?) {
                showAdListener.onOpened()
                adShown(waterfallModel)
            }

            override fun onUnityAdsFinish(p0: String?, p1: UnityAds.FinishState?) {
                p1?.let {
                    if (it != UnityAds.FinishState.SKIPPED) {
                        if (isRewarded) {
                            showAdListener.onRewarded(true)
                            return
                        }
                    }
                }
                showAdListener.onClosed()

            }

            override fun onUnityAdsError(p0: UnityAds.UnityAdsError?, p1: String?) {
                showAdListener.onError(p1 ?: "Unknown Error from UnityAds !!")
                Utils.error(p1 ?: "Unknown Error from UnityAds !!")
                adShown(waterfallModel)

            }
        })
    }

}