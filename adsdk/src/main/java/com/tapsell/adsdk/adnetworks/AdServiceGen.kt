package com.tapsell.adsdk.adnetworks

import com.tapsell.adsdk.adnetworks.chartboostsdk.ChartboostService
import com.tapsell.adsdk.adnetworks.tapsellsdk.TapsellService
import com.tapsell.adsdk.adnetworks.unityadsdk.UnityAdService

internal object AdServiceGen {
    /**
     * AdService hash map
     */
    private val adServiceMap: HashMap<String, AdService> = hashMapOf()

    fun getAdService(serviceName: String): AdService {
        return adServiceMap[serviceName] ?: kotlin.run {
            return@run when (AdServiceEnum.valueOf(serviceName)) {
                AdServiceEnum.Tapsell -> {
                    adServiceMap[serviceName] = TapsellService()
                    adServiceMap[serviceName]!!
                }
                AdServiceEnum.UnityAds -> {
                    adServiceMap[serviceName] = UnityAdService()
                    adServiceMap[serviceName]!!
                }
                AdServiceEnum.Chartboost -> {
                    adServiceMap[serviceName] = ChartboostService()
                    adServiceMap[serviceName]!!
                }
            }
        }
    }
}