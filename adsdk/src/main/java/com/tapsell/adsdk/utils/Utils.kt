package com.tapsell.adsdk.utils

import android.app.Service
import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import android.widget.Toast
import com.tapsell.adsdk.utils.AppConfig.TAG


object Utils {

    fun error(message: String) {
        Log.e(TAG, message)

    }

    fun debug(message: Any) {
        Log.d(TAG, message.toString())
    }

    fun toast(message: Any, context: Context = AppConfig.gContext) {
        Toast.makeText(context, message.toString(), Toast.LENGTH_SHORT).show()

    }

     fun haveNetworkConnection(): Boolean {
        var haveConnectedWifi = false
        var haveConnectedMobile = false
        val cm =  AppConfig.gContext.getSystemService(Service.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val netInfo = cm.allNetworkInfo
        for (ni in netInfo) {
            if (ni.typeName.equals(
                    "WIFI",
                    ignoreCase = true
                )
            ) if (ni.isConnected) haveConnectedWifi = true
            if (ni.typeName.equals(
                    "MOBILE",
                    ignoreCase = true
                )
            ) if (ni.isConnected) haveConnectedMobile = true
        }
        return haveConnectedWifi || haveConnectedMobile
    }

}