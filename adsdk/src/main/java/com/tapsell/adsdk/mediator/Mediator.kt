package com.tapsell.adsdk.mediator

import android.app.Activity

import android.content.Context
import com.tapsell.adsdk.servicemanagers.AdServiceManager


class Mediator {
    companion object {


        private val adManager = AdServiceManager()

        /**
         * STEEP 1 INITIALIZER
         */


        @JvmStatic
        @JvmOverloads
        fun initialize(
            appId: String,
            context: Context,
            chartboostAppSignature: String,
            listener: InitializerResponse? = null
        ) {
            adManager.initialize(appId, context, chartboostAppSignature, listener)
        }


        /**
         *
         * STEEP 2 REQUEST AD
         */
        @JvmStatic
        fun requestAd(zoneId: String, requestAdListener: RequestAdListener) {
            adManager.requestAd(zoneId, requestAdListener)
        }


        /**
         *
         * STEEP 3 SHOW AD
         */

        @JvmStatic
        @JvmOverloads
        fun showAd(zoneId: String? = null, activity: Activity, showAdListener: ShowAdListener) {
            adManager.showAd(zoneId, activity, showAdListener)
        }


    }
}