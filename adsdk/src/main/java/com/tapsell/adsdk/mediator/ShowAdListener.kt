package com.tapsell.adsdk.mediator

interface ShowAdListener {
    fun onError(message:String)
    fun onOpened()
    fun onClosed()
    fun onRewarded(reward: Boolean)

}