package com.tapsell.adsdk.mediator

interface InitializerResponse {

    fun  onComplete()
    fun  onError(message:String)
}