package com.tapsell.adsdk.mediator

interface RequestAdListener {
    fun onRequestAdError(message:String)
    fun onRequestAdComplete()

}