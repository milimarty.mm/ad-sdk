package com.tapsell.adsdk.persistence.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.tapsell.adsdk.api.model.RequestAdModel
import com.tapsell.adsdk.persistence.db.dao.RequestAdDao
import com.tapsell.adsdk.persistence.db.dao.WaterfallDao
import com.tapsell.adsdk.utils.AppConfig

private const val DATABASE_NAME = "ad_sdk_database"

@Database(
    entities = [RequestAdModel::class, RequestAdModel.WaterfallModel::class],
    version = 1,
    exportSchema = false
)
internal abstract class MainDatabase : RoomDatabase() {

    abstract fun adRequestDao(): RequestAdDao
    abstract fun waterfallDao(): WaterfallDao

    companion object {

        @Volatile
        private var INSTANCE: MainDatabase? = null

        fun getDatabase(context: Context = AppConfig.gContext): MainDatabase {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MainDatabase::class.java,
                    DATABASE_NAME
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}