package com.tapsell.adsdk.persistence.db.dao

import androidx.room.*
import com.tapsell.adsdk.api.model.AdNetworksModel
import com.tapsell.adsdk.api.model.RequestAdModel

@Dao
internal interface WaterfallDao {
    @Query("SELECT * FROM waterfall ORDER BY waterfallId ASC")
    fun getAll(): List<RequestAdModel.WaterfallModel>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(waterfallModelRequest: RequestAdModel.WaterfallModel): Long

    @Update
    suspend fun update(waterfallModelRequest: RequestAdModel.WaterfallModel)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertList(waterfallList: List<RequestAdModel.WaterfallModel>)

    @Query("DELETE FROM waterfall")
    suspend fun deleteAll()
    @Delete
    suspend fun delete(waterfallModelRequest: RequestAdModel.WaterfallModel )

    @Query("SELECT * FROM waterfall WHERE waterfall.isAdReady = :isAdReady ORDER BY waterfallId ASC")
    fun getAllByIsAdReady(isAdReady:Boolean=true): List<RequestAdModel.WaterfallModel>

    @Query("SELECT * FROM waterfall WHERE waterfall.adNetwork = :adNetworksModel ORDER BY waterfallId ASC")
    fun getAllByAdNetworkId(adNetworksModel: String): List<RequestAdModel.WaterfallModel>
}