package com.tapsell.adsdk.persistence.db.entity

import androidx.room.Embedded
import androidx.room.Relation
import com.tapsell.adsdk.api.model.RequestAdModel


internal data class AdRequestWithWaterfall(
    @Embedded
    val RequestAdModel: RequestAdModel,
    @Relation(
        parentColumn = "adRequestId",
        entityColumn = "adRequestId",
        entity = RequestAdModel.WaterfallModel::class
    )
    var waterfallModelListRequest: List<RequestAdModel.WaterfallModel>


)