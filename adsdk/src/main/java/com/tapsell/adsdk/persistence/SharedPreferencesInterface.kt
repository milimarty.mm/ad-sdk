package com.tapsell.adsdk.persistence

import kotlinx.coroutines.flow.Flow

internal interface SharedPreferencesInterface<T> {
    fun getItems():T
    fun saveItem(item:T)
}