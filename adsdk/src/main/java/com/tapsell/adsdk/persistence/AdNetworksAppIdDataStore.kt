package com.tapsell.adsdk.persistence

import android.content.Context
import android.content.SharedPreferences
import com.tapsell.adsdk.api.model.AdNetworksModel
import com.tapsell.adsdk.utils.AppConfig

private const val AD_NETWORKS_PREFERENCE_NAME = "ad_networks_preference"

internal class AdNetworksAppIdDataStore(context: Context = AppConfig.gContext) :
    SharedPreferencesInterface<AdNetworksModel.AdNetworksAppIdModel> {

    private val tapsellAppId = "Tapsell"
    private val unityAdsAppId = "UnityAds"
    private val chartboostAppId = "Chartboost"
    private var sharedPreferences: SharedPreferences? = null


    override fun saveItem(item: AdNetworksModel.AdNetworksAppIdModel) {
        item.let {
            sharedPreferences!!.edit().apply {
                putString(tapsellAppId, it.tapsellAppId)
                putString(chartboostAppId, it.charBoostAppId)
                putString(unityAdsAppId, it.unityAdsAppId)
                apply()
            }
        }

    }

    override fun getItems(): AdNetworksModel.AdNetworksAppIdModel {
        return AdNetworksModel.AdNetworksAppIdModel(
            sharedPreferences?.getString(tapsellAppId, "") ?: "",
            sharedPreferences?.getString(unityAdsAppId, "") ?: "",
            sharedPreferences?.getString(chartboostAppId, "") ?: "",

            )

    }

    init {
        sharedPreferences =
            context.getSharedPreferences(AD_NETWORKS_PREFERENCE_NAME, Context.MODE_PRIVATE)
    }
}