package com.tapsell.adsdk.persistence.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tapsell.adsdk.api.model.RequestAdModel
import com.tapsell.adsdk.persistence.db.entity.AdRequestWithWaterfall

@Dao
internal interface RequestAdDao {

    @Query("SELECT * FROM ad_request ORDER BY adRequestId ASC")
    fun getAll(): List<RequestAdModel>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(requestAdEntity: RequestAdModel):Long

    @Query("DELETE FROM ad_request")
    suspend fun deleteAll()

    @Query("SELECT * FROM ad_request INNER JOIN waterfall ON ad_request.adRequestId = waterfall.adRequestId GROUP BY ad_request.adRequestId")
    fun getAdRequestWithWaterfall():List<AdRequestWithWaterfall>

}