package com.tapsell.adsdk.persistence

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

internal object DbScope {
    fun <T> transaction(job: suspend () -> T?, done: (T?) -> Unit) {
        CoroutineScope(Dispatchers.Main).launch {
            val result = async(Dispatchers.IO) {
                return@async job()
            }.await()
            done(result)
        }
    }
}