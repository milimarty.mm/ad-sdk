package com.tapsell.adsdk.api.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.tapsell.adsdk.adnetworks.AdServiceEnum
import kotlinx.android.parcel.Parcelize
import kotlinx.parcelize.IgnoredOnParcel


internal data class AdNetworksModel(
    @SerializedName("adNetworks")
    var adAdNetworksAppIdModel: AdNetworksAppIdModel
) {
    internal data class AdNetworksAppIdModel(
        @SerializedName("Tapsell")
        var tapsellAppId: String?,
        @SerializedName("UnityAds")
        var unityAdsAppId: String?,
        @SerializedName("Chartboost")
        var charBoostAppId: String?,
        var chartBoostAppSignature: String = ""
    ) {

        val serviceApIdList
            get() = arrayListOf(
                Pair(AdServiceEnum.Tapsell, tapsellAppId),
                Pair(AdServiceEnum.UnityAds, unityAdsAppId),
                Pair(AdServiceEnum.Chartboost, charBoostAppId)
            )
    }
}