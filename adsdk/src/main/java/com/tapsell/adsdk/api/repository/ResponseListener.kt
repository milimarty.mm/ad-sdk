package com.tapsell.adsdk.api.repository

internal interface ResponseListener<T> {
    fun onSuccess(data:T)
    fun onError(message:String)

}