package com.tapsell.adsdk.api

import com.google.gson.GsonBuilder
import com.tapsell.adsdk.utils.AppConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


internal object ApiAdapter {


    private fun provideRetrofit(): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        return Retrofit.Builder().baseUrl(AppConfig.SERVER_ADDRESS)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    private fun getRestApi(): RestApi =
        provideRetrofit().create(RestApi::class.java)


    fun getInitializeConfig(appId: String) = getRestApi().getInitializeConfig(appId)

    suspend fun getAdRequestConfig(zoneId: String) = getRestApi().getAdRequestConfig(zoneId)
}