package com.tapsell.adsdk.api.repository

import android.util.Log
import com.tapsell.adsdk.api.ApiAdapter
import com.tapsell.adsdk.api.model.AdNetworksModel
import com.tapsell.adsdk.api.model.RequestAdModel
import com.tapsell.adsdk.persistence.AdNetworksAppIdDataStore
import com.tapsell.adsdk.persistence.DbScope
import com.tapsell.adsdk.persistence.db.MainDatabase
import com.tapsell.adsdk.persistence.db.dao.RequestAdDao
import com.tapsell.adsdk.persistence.db.dao.WaterfallDao
import com.tapsell.adsdk.utils.Utils
import kotlinx.coroutines.runBlocking

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

internal object ServicesRepository {
    private val requestAdDao: RequestAdDao
        get() = MainDatabase.getDatabase().adRequestDao()
    private val waterfallDao: WaterfallDao
        get() = MainDatabase.getDatabase().waterfallDao()


    /**
     * get Initialize config from server and save it to data store and start to init ad networks
     */
    fun getInitializeConfig(
        appId: String,
        chartboostAppSignature: String,
        result: ResponseListener<AdNetworksModel>
    ) {
        ApiAdapter.getInitializeConfig(appId).enqueue(object : Callback<AdNetworksModel> {
            override fun onResponse(
                call: Call<AdNetworksModel>,
                response: Response<AdNetworksModel>
            ) {
                if (response.isSuccessful) {
                    Log.d("RESPONSE: ", response.body().toString())
                    response.body()?.let {
                        it.adAdNetworksAppIdModel.chartBoostAppSignature = chartboostAppSignature
                        result.onSuccess(it)
                        AdNetworksAppIdDataStore().saveItem(it.adAdNetworksAppIdModel)
                    } ?: kotlin.run {
                        Utils.error("invalid response object")
                        result.onError("invalid response object")

                    }

                }

            }

            override fun onFailure(call: Call<AdNetworksModel>, t: Throwable) {

                t.message?.let {
                    Utils.error(it)
                    result.onError(it)
                } ?: kotlin.run {
                    Utils.error("There is a unknown error !! ")
                    result.onError("There is a unknown error !! ")
                }
            }
        })

    }

    /**
     * get AdRequest config and waterfall and cache it in database
     */

    private suspend fun getAdRequestConfigFromApi(
        zoneId: String,
        result: ResponseListener<RequestAdModel>
    ) {
        try {
            ApiAdapter.getAdRequestConfig(zoneId).apply {
                ttl += Date().time
                val adRequestId = requestAdDao.insert(this)
                waterfallModel.forEach { waterfallItem ->
                    waterfallItem.adRequestId = adRequestId
                }
                waterfallDao.insertList(waterfallModel)
                result.onSuccess(this)
                return
            }

        } catch (exception: Exception) {
            result.onError(exception.message.toString())
            Utils.error(exception.message.toString())
            return
        }


    }

    fun getAdRequestConfig(zoneId: String, result: ResponseListener<RequestAdModel>) {
        DbScope.transaction({
            requestAdDao.getAdRequestWithWaterfall().takeIf { it.isNotEmpty() }?.let { list ->
                val item = list[0]
                if (item.RequestAdModel.ttl <= Date().time) {
                    requestAdDao.deleteAll()
                    waterfallDao.deleteAll()
                    return@transaction getAdRequestConfigFromApi(zoneId, result)
                } else {
                    item.RequestAdModel.apply {
                        waterfallModel = ArrayList(item.waterfallModelListRequest)
                        result.onSuccess(this)
                        return@transaction
                    }
                }

            }
            return@transaction getAdRequestConfigFromApi(zoneId, result)
        }, {})

    }


    fun updateWaterfallByAdNetworkId(adId: String, adNetworkId: String) = runBlocking {
        DbScope.transaction({
            waterfallDao.getAllByIsAdReady().takeIf { it.isNotEmpty() }?.let {
                it[0].apply {
                    this.adId = ""
                    isAdReady = false
                    waterfallDao.update(this)
                }

            }
            waterfallDao.getAllByAdNetworkId(adNetworkId).takeIf { it.isNotEmpty() }?.let { list ->
                list[0]
                    .apply {
                        this.adId = adId
                        isAdReady = true
                    }
                waterfallDao.update(list[0])
            }
        }, {})
    }

    fun updateWaterFall(waterfallModel: RequestAdModel.WaterfallModel) = runBlocking {
        DbScope.transaction({
            waterfallDao.update(waterfallModel)

        }, {})
    }

    fun getAllByIsAdReady(function: (RequestAdModel.WaterfallModel?) -> Unit) {
        DbScope.transaction({
            MainDatabase.getDatabase().waterfallDao().getAllByIsAdReady().takeIf { it.isNotEmpty() }
                ?.let {
                    Utils.debug("----------$it")
                    return@transaction it[0]
                } ?: kotlin.run {
                return@transaction null
            }
        }, {
            function(it)
        })
    }


}