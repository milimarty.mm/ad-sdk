package com.tapsell.adsdk.api.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import org.jetbrains.annotations.NotNull

@Parcelize
@Entity(tableName = "ad_request")
internal data class RequestAdModel(
    @SerializedName("zoneType")
    var zoneType: String,
    @SerializedName("waterfall")
    @Ignore
    var waterfallModel: ArrayList<WaterfallModel>,
    @SerializedName("ttl")
    var ttl: Long,
    @PrimaryKey(autoGenerate = true)
    @NotNull
    var adRequestId: Long = 0,
) : Parcelable {
    constructor() : this(zoneType = "", ttl = 0, waterfallModel = arrayListOf())

    @Parcelize
    @Entity(tableName = "waterfall")
    internal data class WaterfallModel(
        @SerializedName("adNetwork")
        var adNetwork: String,
        @SerializedName("zoneId")
        var zoneId: String,
        @SerializedName("timeout")
        var timeout: Int,
        var adId: String="",
        var isAdReady: Boolean = false,
        @PrimaryKey(autoGenerate = true)
        @NotNull
        var waterfallId: Long = 0,
        var adRequestId: Long = 0,
    ) : Parcelable {
        constructor() : this(adNetwork = "", zoneId = "", timeout = 0)

    }


}
