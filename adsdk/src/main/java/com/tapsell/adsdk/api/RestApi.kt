package com.tapsell.adsdk.api

import com.tapsell.adsdk.api.model.AdNetworksModel
import com.tapsell.adsdk.api.model.RequestAdModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

internal interface RestApi {


    @GET("init/")
    fun getInitializeConfig(@Query("appId") appId: String): Call<AdNetworksModel>

    @GET("adRequest/")
    suspend fun getAdRequestConfig(@Query("zoneId") zoneId: String): RequestAdModel



}