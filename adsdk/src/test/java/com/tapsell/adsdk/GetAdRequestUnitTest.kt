package com.tapsell.adsdk

import com.google.gson.Gson
import com.tapsell.adsdk.api.ApiAdapter
import com.tapsell.adsdk.api.model.RequestAdModel
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.net.HttpURLConnection

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class GetAdRequestUnitTest {
    lateinit var mockWebServer: MockWebServer

    private val requestAdModel: RequestAdModel =
        RequestAdModel(
            ttl = 3600000,
            zoneType = "Interstitial",
            waterfallModel = arrayListOf(
                RequestAdModel.WaterfallModel(
                    adNetwork = "UnityAds",
                    zoneId = "zoneIdInUnityAds",
                    timeout = 2000

                ),
                RequestAdModel.WaterfallModel(
                    adNetwork = "Tapsell",
                    zoneId = "zoneIdInTapsell",
                    timeout = 3000

                ),
                RequestAdModel.WaterfallModel(
                    adNetwork = "Chartboost",
                    zoneId = "zoneIdInChartboost",
                    timeout = 1000

                )
            )

        )
    private val successResponse: String = Gson().toJson(requestAdModel)

    @Before
    fun setUp() {

        mockWebServer = MockWebServer()
        mockWebServer.start()
    }


//    @Test
//    fun `fetch details and check response Code 200 returned`() = runBlocking {
//
//
//        // Assign
//        val response = MockResponse()
//            .setResponseCode(HttpURLConnection.HTTP_OK)
//            .setBody(successResponse)
//        mockWebServer.enqueue(response)
//        // Act
//
//        val actualResponse = ApiAdapter.getAdRequestConfig("")
//        // Assert
//        assertEquals(
//            response.toString().contains("200"),
//            actualResponse.code().toString().contains("200")
//        )
//    }

    @Test
    fun `fetch details and check response success returned`() = runBlocking {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(successResponse)
        mockWebServer.enqueue(response)
        val mockResponse = response.getBody()?.readUtf8()
        // Act
        val actualResponse = ApiAdapter.getAdRequestConfig("")
        // Assert
        assertEquals(mockResponse?.let {
            Gson().fromJson(
                it,
                RequestAdModel::class.java
            ).ttl
        }, actualResponse.ttl)
    }


    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}