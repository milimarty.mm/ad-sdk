package com.tapsell.adsdk

import com.google.gson.Gson
import com.tapsell.adsdk.api.model.AdNetworksModel
import com.tapsell.adsdk.api.ApiAdapter
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.net.HttpURLConnection

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class GetInitUnitTest {
    lateinit var mockWebServer: MockWebServer
    private val adNetworksModel: AdNetworksModel =
        AdNetworksModel(
            AdNetworksModel.AdNetworksAppIdModel(
                tapsellAppId = "fkngkebhoctbrlresdogasirgqhephasbbnpigkmidndendsnfqftsclkbrrrjnjisknnd",
                unityAdsAppId = "3990075",
                charBoostAppId = "appIdInChartboost"

            )
        )
    private val successResponse: String = Gson().toJson(adNetworksModel)

    @Before
    fun setUp() {


        mockWebServer = MockWebServer()
        mockWebServer.start()
    }


    @Test
    fun `fetch details and check response Code 200 returned`() {


        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(successResponse)
        mockWebServer.enqueue(response)
        // Act

        val actualResponse = ApiAdapter.getInitializeConfig("").execute()
        // Assert
        assertEquals(
            response.toString().contains("200"),
            actualResponse.code().toString().contains("200")
        )
    }

    @Test
    fun `fetch details and check response success returned`() {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(successResponse)
        mockWebServer.enqueue(response)
        val mockResponse = response.getBody()?.readUtf8()
        // Act
        val actualResponse = ApiAdapter.getInitializeConfig("").execute()
        // Assert
        assertEquals(mockResponse?.let { Gson().fromJson(it,AdNetworksModel::class.java).adAdNetworksAppIdModel.tapsellAppId }, actualResponse.body()?.adAdNetworksAppIdModel!!.tapsellAppId)
    }


    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}