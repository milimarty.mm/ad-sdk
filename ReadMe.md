

 # راه اندازی  Ad Sdk در اندروید

 ## تنظیمات Gradle
خطوط زیر را به فایل `build.gradle` کل پروژه در قسمت `allprojects -> repositories` اضافه کنید.

```Java
flatDir {
   dirs 'libs'
}
```

سپس فایل `adsdk-release.aar` را در قسمت `app -> libs`قرار دهید.

خط زیر را به فایل ` build.gradle` ماژول برنامه در قسمت`dependencies` اضافه کنید.

```Java

    implementation(name:'adsdk-release',ext:'aar')
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.9'
    implementation "com.google.android.gms:play-services-base:17.5.0"
    implementation "com.google.android.gms:play-services-identity:17.0.0"

 ```
 سپس |عملیات Sync کردن را انجام دهید.

 # راه‌اندازی AdSdk
 کد زیر در کلاس `Application` پیاده سازی کنید.

 ```Kotlin
 import com.tapsell.adsdk.mediator.Mediator
 ...
 public void onCreate() {
    super.onCreate();
      Mediator.initialize(
                APP_ID,
                applicationContext,
                CHARTBOOST_SIGN,
            )
 }
```
`APP_ID` شناسه اپ شما می باشد.

## تنظیمات Network Security Configuration
در صورتی که اپلیکیشن شما درخواست‌های `http` ارسال می‌کند، در مسیر `res/xml` پروژه‌ی خود یک فایل به نام     `network_security_config.xml` بسازید.

* اگر تمام ارتباطات اپلیکیشن شما از طریق پروتوکل http برقرار می‌شود، خطوط زیر را به فایل `network_security_config.xml` اضافه کنید:

```xml
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
  <base-config cleartextTrafficPermitted="true"/>
  </base-config>
</network-security-config>
```
*اگر اپلیکیشن شما به تعداد محدودی از `domain `ها بسته‌های `http` ارسال می‌کند، خطوط زیر را به فایل `network_security_config.xml` اضافه کنید:

```xml
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
  <domain-config cleartextTrafficPermitted="true"/>
    <domain includeSubdomains="true"><!—your subdomain--></domain>
  </domain-config>
</network-security-config>
```
در آخر به تگ `application` در فایل `AndroidManifest.xml` اپلیکیشن خود، `attribute` `android:networkSecurityConfig` را مطابق خطوط زیر اضافه کنید:
```xml
<?xml version="1.0" encoding="utf-8"?>
<manifest>
    <application
        android:networkSecurityConfig="@xml/network_security_config">
    </application>
</manifest>
```
و در صورت وجود `attribute` زیر در تگ `application` در `AndroidManifest.xml` آن را حذف کنید:
```xml
<application
         android:usesCleartextTraffic="true">
```
### درخواست تبلیغ
```Java
  Mediator.requestAd(ZONE_ID, object : RequestAdListener {
                override fun onRequestAdError(message: String) {
                }

                override fun onRequestAdComplete() {

                }

            })
```
ورودی اول `ZONE_ID` برابر با شناسه تبلیغ‌گاهی هست که در پنل ساخته‌اید.
ورودی دوم از نوع `RequestAdListener` است.

متدهای `RequestAdListener` مطابق جدول زیر است و نتیجه درخواست تبلیغ با کمک این بخش برمیگردد.
| متد |عملکرد |
|----|----|
|onRequestAdComplete|تبلیع آماده نمایش است|
|onRequestAdError|خطایی رخ داده از طریق `message` می‌توانید خطا را ببینید.|
### نمایش تبلیغ
برای نمایش تبلیغ از تابغ زیر استفاده کنید.
```Java
Mediator.showAd(ZONE_ID
                activity = this,
                showAdListener = object : ShowAdListener {
                    override fun onError(message: String) {


                    }

                    override fun onOpened() {


                    }

                    override fun onClosed() {


                    }

                    override fun onRewarded(reward: Boolean) {

                    }


                })
```
ورودی اول `ZONE_ID` که پارمتر اختیاری هست که در صورت ارسال نکردن پارمتر مربوط به این سرویس که قبلا کش شده بود قرار خواهد گرفت.
ورودی دوم `Activity` که در آن هستید را ارسال میکنید.
ورودی سوم `ShowAdListener` که وضعیت تبلغ را نمایش می دهد.

| متد |عملکرد |
|----|----|
|onOpened|هنگام باز شدن تبلیغ صدا زده میشود.	|
|onClosed|هنگام بسته شدن تبلیغ صدا زده میشود.|
|onError|خطایی رخ داده از طریق `message` می‌توانید خطا را ببینید.|
|onRewarded|وضعیت دریافت جایزه در تبلیغات جایزه‌ای را نشان میدهد.|





